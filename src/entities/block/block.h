#ifndef BLOCK_H
#define BLOCK_H

#include "raylib.h"
#include "entities/game_object/game_object.h"

namespace sokoban
{
	namespace block
	{
		using namespace game_object;

		enum class BLOCK_STATE
		{
			OFF_DESTINATION = 1,
			ON_DESTINATION,
			FINISH
		};

		void init();
		void add(int i, int j);
		bool checkBlock(TileIndex auxIndex);
		void update();
		void draw();
		void clear();

		class Block : public GameObject
		{
		public:
			Block();
			Block(TileIndex index, Rectangle rec, float speed, BLOCK_STATE state);
			~Block();

			void move();
			void show();
			void setStateForTile();
			void changeState(BLOCK_STATE state);

			Texture2D getTexture();
			void setTexture(Texture2D texture);
			BLOCK_STATE GetBlockState();
			void setBlockState(BLOCK_STATE blockState);

		private:
			Texture2D texture;
			BLOCK_STATE blockState;
		};
	}
}

#endif // !BLOCK_H