#include "block.h"
#include "audio/audio.h"
#include "texture/texture.h"
#include "scenes/gameplay/grid/grid.h"

namespace sokoban
{
	namespace block
	{
		float blockWidth;
		float blockHeight;

		void init()
		{
			blockWidth = tile::tileWidth;
			blockHeight = tile::tileHeight;
		}

		void add(int i, int j)
		{
			Rectangle auxRec = Rectangle();
			auxRec.width = blockWidth;
			auxRec.height = blockHeight;
			auxRec.x = (grid::tiles[i][j]->getRec().x + (grid::tiles[i][j]->getRec().width / 2)) - (blockWidth / 2);
			auxRec.y = (grid::tiles[i][j]->getRec().y + (grid::tiles[i][j]->getRec().height / 2)) - (blockHeight / 2);

			grid::blocks.push_back(new Block({ i, j }, auxRec, speedObject, BLOCK_STATE::OFF_DESTINATION));
		}

		bool checkBlock(TileIndex auxIndex)
		{
			for (block::Block* b : grid::blocks)
			{
				if (b != NULL)
				{
					if (b->getIndex().i == auxIndex.i && b->getIndex().j == auxIndex.j)
					{
						return true;
					}
				}
			}

			return false;
		}

		void update()
		{
			for (Block* b : grid::blocks)
			{
				if (b != NULL)
				{
					b->move();
					b->setStateForTile();
				}
			}
		}

		void draw()
		{
			for (Block* b : grid::blocks)
			{
				if (b != NULL)
				{
					b->show();
				}
			}
		}

		void clear()
		{
			for (Block* b : grid::blocks)
			{
				if (b != NULL)
				{
					delete b;
					b = NULL;
				}
			}

			grid::blocks.clear();
		}

		Block::Block() : GameObject()
		{
			this->texture = Texture2D();
			this->blockState = BLOCK_STATE();
		}

		Block::Block(TileIndex index, Rectangle rec, float speed, BLOCK_STATE state)
			: GameObject(rec, speed)
		{
			this->index = index;
			this->texture = texture;
			changeState(state);
		}

		Block::~Block()
		{
		}

		void Block::move()
		{
			GameObject::move();

			if (isMove)
			{
				if (!IsSoundPlaying(audio::blockSound))
				{
					PlaySound(audio::blockSound);
				}
			}
		}

		void Block::show()
		{
			if (blockState != BLOCK_STATE::FINISH)
			{
				DrawTextureEx(texture, getPosition(), 0.0f, 1.0f, color);
			}
			else
			{
				Vector2 pos = { getPosition().x, getPosition().y - rec.height * 20 / 100 };
				DrawTextureEx(texture, pos, 0.0f, 1.0f, color);
			}

			GameObject::show();
		}

		void Block::setStateForTile()
		{
			bool checkTile = false;

			for (int i = 0; i < grid::gridRow; i++)
			{
				for (int j = 0; j < grid::gridCol; j++)
				{
					if (grid::tiles[i][j]->getType() == tile::TILE_TYPE::DEST_BLOCK)
					{
						if (index.i == i && index.j == j)
						{
							if (blockState == block::BLOCK_STATE::OFF_DESTINATION)
							{
								changeState(block::BLOCK_STATE::ON_DESTINATION);
								
								if (!IsSoundPlaying(audio::openChestSound))
								{
									PlaySound(audio::openChestSound);
								}
							}

							checkTile = true;

							i = grid::gridRow;
							break;
						}
					}
				}
			}

			if (!checkTile)
			{
				if (blockState == block::BLOCK_STATE::ON_DESTINATION)
				{
					changeState(block::BLOCK_STATE::OFF_DESTINATION);
				}
			}
		}

		void Block::changeState(BLOCK_STATE state)
		{
			int width = static_cast<int>(rec.width);
			int height = static_cast<int>(rec.height);

			switch (state)
			{
			case BLOCK_STATE::OFF_DESTINATION:
				texture = texture::blockOff;
				break;
			case BLOCK_STATE::ON_DESTINATION:
				texture = texture::blockOn;
				break;
			case BLOCK_STATE::FINISH:
				texture = texture::blockFinish;
				height += static_cast<int>(rec.height * 20 / 100);
				break;
			default:
				break;
			}

			texture.width = width;
			texture.height = height;

			blockState = state;
		}

		Texture2D Block::getTexture()
		{
			return this->texture;
		}

		void Block::setTexture(Texture2D texture)
		{
			this->texture = texture;
		}

		BLOCK_STATE Block::GetBlockState()
		{
			return this->blockState;
		}

		void Block::setBlockState(BLOCK_STATE blockState)
		{
			this->blockState = blockState;
		}
	}
}