#ifndef ANIMATION_H
#define ANIMATION_H

#include "raylib.h"

namespace sokoban
{
	namespace animation
	{
		class Animation
		{
		public:
			Animation();
			Animation(Texture2D texture, Rectangle framRec, int frames, float speed, int resets, bool isLoop);
			~Animation();

			void update();
			void draw(Vector2 position, Color color);

			void setTexture(Texture2D texture);
			Texture2D getTexture();
			void setFrameRec(Rectangle frameRec);
			Rectangle getFrameRec();
			void setFrames(int frames);
			int getFrames();
			void setCurrentFrame(int currentFrame);
			int getCurrentFrame();
			void setFrameCounter(float frameCounter);
			float getFrameCounter();
			void setSpeed(float speed);
			float getSpeed();
			void setResets(int resets);
			int getResets();
			void setIsLoop(bool isLoop);
			bool getIsLoop();
			void setIsFinish(bool isFinish);
			bool getIsFinish();
		private:
			Texture2D texture;
			Rectangle frameRec;
			int frames;
			int currentFrame;
			float frameCounter;
			float speed;
			int resets;
			bool isLoop;
			bool isFinish;
		};
	}
}

#endif // !ANIMATION_H