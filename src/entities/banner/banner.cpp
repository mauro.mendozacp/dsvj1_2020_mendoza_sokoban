#include "banner.h"
#include "input/input.h"
#include "game/game.h"

namespace sokoban
{
	namespace banner
	{
		float optionRecWidth = 300.0f;
		float optionRecHeight = 50.0f;

		float buttonRecWidth = 50.0f;
		float buttonRecHeight = 50.0f;

		float menuRecWidth = static_cast<float>(game::screenWidth * 3 / 4);
		float menuRecHeight = static_cast<float>(game::screenHeight * 3 / 4);

		Banner::Banner()
		{
			this->position = Vector2();
			this->rec = Rectangle();
			this->texture = Texture2D();
			this->text = "vacio";
			this->font = Font();
			this->fontSize = 0.0f;
			this->spacing = 0.0f;
			this->color = Color();
		}

		Banner::Banner(Vector2 position, Rectangle rec, Texture2D texture, std::string text)
		{
			this->position = position;
			this->rec = rec;
			this->texture = texture;
			this->text = text;
			this->font = font::font;
			this->fontSize = font::size;
			this->spacing = font::spacing;
			this->color = font::color;
		}

		Banner::Banner(Vector2 position, Rectangle rec, Texture2D texture, std::string text, float fontSize, float spacing)
		{
			this->position = position;
			this->rec = rec;
			this->texture = texture;
			this->text = text;
			this->font = font::font;
			this->fontSize = fontSize;
			this->spacing = spacing;
			this->color = font::color;
		}

		Banner::~Banner()
		{
		}

		void Banner::show()
		{
			DrawTextureRec(texture, rec, position, color);

#if DEBUG
			DrawRectangleLinesEx({ position.x,position.y,rec.width,rec.height }, 1, GREEN);
#endif // DEBUG
		}

		void Banner::showMid()
		{
			show();

			Vector2 pos;
			pos.x = ((position.x + rec.width / 2) - (MeasureTextEx(font, &text[0], fontSize, spacing).x / 2));
			pos.y = ((position.y + rec.height / 2) - (MeasureTextEx(font, &text[0], fontSize, spacing).y / 2) - 3);

			DrawTextEx(font, &text[0], pos, fontSize, spacing, color);
		}

		void Banner::showTop()
		{
			show();

			Vector2 pos;
			pos.x = ((position.x + rec.width / 2) - (MeasureTextEx(font, &text[0], fontSize, spacing).x / 2));
			pos.y = ((position.y + rec.height / 16) - (MeasureTextEx(font, &text[0], fontSize, spacing).y / 2) - 3);

			DrawTextEx(font, &text[0], pos, fontSize, spacing, color);
		}

		void Banner::showMidTop()
		{
			show();

			Vector2 pos;
			pos.x = ((position.x + rec.width / 2) - (MeasureTextEx(font, &text[0], fontSize, spacing).x / 2));
			pos.y = ((position.y + rec.height * 3 / 8) - (MeasureTextEx(font, &text[0], fontSize, spacing).y / 2) - 3);

			DrawTextEx(font, &text[0], pos, fontSize, spacing, color);
		}

		void Banner::showMidRight()
		{
			show();

			Vector2 pos;
			pos.x = (position.x + rec.width / 2);
			pos.y = ((position.y + rec.height / 2) - (MeasureTextEx(font, &text[0], fontSize, spacing).y / 2) - 3);

			DrawTextEx(font, &text[0], pos, fontSize, spacing, color);
		}

		void Banner::changeState()
		{
			if (checkMouseOnRec())
			{
				rec.y = rec.height;
			}
			else if (rec.y != 0.0f)
			{
				rec.y = 0.0f;
			}
		}

		void Banner::setState(bool on)
		{
			if (on)
			{
				rec.y = 0.0f;
			}
			else
			{
				rec.y = rec.height;
			}
		}

		bool Banner::checkMouseOnRec()
		{
			if (CheckCollisionPointRec(GetMousePosition(), getRecPosition()))
			{
				return true;
			}

			return false;
		}

		void Banner::setRec(Rectangle rec)
		{
			this->rec = rec;
		}

		Rectangle Banner::getRec()
		{
			return this->rec;
		}

		void Banner::setPosition(Vector2 position)
		{
			this->position = position;
		}

		Vector2 Banner::getPosition()
		{
			return this->position;
		}

		Rectangle Banner::getRecPosition()
		{
			return { position.x, position.y, rec.width, rec.height };
		}

		void Banner::setTexture(Texture2D texture)
		{
			this->texture = texture;
		}

		Texture2D Banner::getTexture()
		{
			return this->texture;
		}

		void Banner::setText(std::string text)
		{
			this->text = text;
		}

		std::string Banner::getText()
		{
			return this->text;
		}

		void Banner::setFont(Font font)
		{
			this->font = font;
		}

		Font Banner::getFont()
		{
			return this->font;
		}

		void Banner::setFontSize(float fontSize)
		{
			this->fontSize = fontSize;
		}

		float Banner::setFontSize()
		{
			return this->fontSize;
		}

		void Banner::setSpacing(float spacing)
		{
			this->spacing = spacing;
		}

		float Banner::getSpacing()
		{
			return this->spacing;
		}

		void Banner::setColor(Color color)
		{
			this->color = color;
		}

		Color Banner::getColor()
		{
			return this->color;
		}

		void initMenu(Banner*& menu, std::string title)
		{
			//Menu
			Vector2 pos = { static_cast<float>(game::screenWidth / 2) - (menuRecWidth / 2),
				static_cast<float>(game::screenHeight / 2) - (menuRecHeight / 2) };

			Rectangle bgTexture = { 0, 0, menuRecWidth, menuRecHeight };

			Texture2D texture = texture::menuBanner;
			texture.width = static_cast<int>(menuRecWidth);
			texture.height = static_cast<int>(menuRecHeight);

			menu = new Banner(pos, bgTexture, texture, title);
		}

		void gameplayButtonsInit()
		{
			float buttonspacing = 20.0f;

			Texture2D auxTexture = texture::pauseButton;
			auxTexture.width = static_cast<int>(buttonRecWidth);
			auxTexture.height = static_cast<int>(buttonRecHeight * 2);

			gameplay::pause = new Banner(
				{ game::screenWidth - buttonRecWidth - buttonspacing, buttonspacing },
				{ 0, 0, buttonRecWidth, buttonRecHeight }, auxTexture, "");

			auxTexture = texture::resetButton;
			auxTexture.width = static_cast<int>(buttonRecWidth);
			auxTexture.height = static_cast<int>(buttonRecHeight * 2);

			gameplay::reset = new Banner(
				{ buttonspacing, buttonspacing },
				{ 0, 0, buttonRecWidth, buttonRecHeight }, auxTexture, "");
		}

		void deInitBanner(Banner*& banner)
		{
			if (banner != NULL)
			{
				delete banner;
				banner = NULL;
			}
		}

		void init(Banner* options[], int optionLenght, std::string optionTexts[])
		{
			//Options
			Vector2 pos = { static_cast<float>(game::screenWidth / 2) - (optionRecWidth / 2),
				static_cast<float>(game::screenHeight / 2) - (optionRecHeight / 2) };

			Rectangle bgTexture = { 0, 0, optionRecWidth, optionRecHeight };

			Texture2D texture = texture::optionBanner;
			texture.width = static_cast<int>(optionRecWidth);
			texture.height = static_cast<int>(optionRecHeight * 2);

			for (int i = 0; i < optionLenght; i++)
			{
				options[i] = new Banner(pos, bgTexture, texture, optionTexts[i]);

				pos.y += optionRecHeight * 3 / 2;
			}
		}

		int getOption(Banner* options[], int optionLenght)
		{
			for (int i = 0; i < optionLenght; i++)
			{
				if (input::checkClickOnRec(options[i]->getRecPosition()))
				{
					return i + 1;
				}
			}

			return 0;
		}

		void update(Banner* options[], int optionLenght)
		{
			for (int i = 0; i < optionLenght; i++)
			{
				options[i]->changeState();
			}
		}

		void draw(Banner* options[], int optionLenght)
		{
			for (int i = 0; i < optionLenght; i++)
			{
				options[i]->showMid();
			}
		}

		void deInit(Banner* options[], int optionLenght)
		{
			for (int i = 0; i < optionLenght; i++)
			{
				if (options[i] != NULL)
				{
					delete options[i];
					options[i] = NULL;
				}
			}
		}
}
}