#ifndef BUTTON_H
#define BUTTON_H

#include "raylib.h"
#include <iostream>

namespace sokoban
{
	namespace banner
	{
		extern float optionRecWidth;
		extern float optionRecHeight;

		extern float buttonRecWidth;
		extern float buttonRecHeight;

		extern float menuRecWidth;
		extern float menuRecHeight;

		class Banner
		{
		public:
			Banner();
			Banner(Vector2 position, Rectangle rec, Texture2D texture, std::string text);
			Banner(Vector2 position, Rectangle rec, Texture2D texture, std::string text, float fontSize, float spacing);
			~Banner();

			void show();
			void showMid();
			void showTop();
			void showMidTop();
			void showMidRight();
			void changeState();
			void setState(bool on);
			bool checkMouseOnRec();

			void setRec(Rectangle rec);
			Rectangle getRec();
			void setPosition(Vector2 position);
			Vector2 getPosition();
			Rectangle getRecPosition();
			void setTexture(Texture2D texture);
			Texture2D getTexture();
			void setText(std::string text);
			std::string getText();
			void setFont(Font font);
			Font getFont();
			void setFontSize(float fontSize);
			float setFontSize();
			void setSpacing(float spacing);
			float getSpacing();
			void setColor(Color color);
			Color getColor();

		private:
			Vector2 position;
			Rectangle rec;
			Texture2D texture;
			std::string text;
			Font font;
			float fontSize;
			float spacing;
			Color color;
		};

		void initMenu(Banner*& menu, std::string title);
		void deInitBanner(Banner*& menu);
		void gameplayButtonsInit();
		void init(Banner* options[], int optionLenght, std::string optionTexts[]);
		int getOption(Banner* options[], int optionLenght);
		void update(Banner* options[], int optionLenght);
		void draw(Banner* options[], int optionLenght);
		void deInit(Banner* options[], int optionLenght);
	}
}

#endif // !BUTTON_H