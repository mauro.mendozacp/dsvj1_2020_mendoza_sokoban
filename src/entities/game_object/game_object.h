#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include "raylib.h"
#include "entities/tileIndex/tileIndex.h"

namespace sokoban
{
	namespace game_object
	{
		const float speedObject = 200.f;

		class GameObject
		{
		public:
			GameObject();
			GameObject(Rectangle rec, float speed);
			~GameObject();

			bool checkMove(TileIndex auxIndex);
			void changeIndexPosition(TileIndex auxIndex);
			virtual void move();
			virtual void show();

			TileIndex getIndex();
			void setIndex(TileIndex index);
			Rectangle getRec();
			void setRec(Rectangle rec);
			Vector2 getPosition();
			void setPosition(Vector2 position);
			float getSpeed();
			void setSpeed(float speed);
			Vector2 getDestinyPosition();
			void setDestinyPosition(Vector2 destinyPosition);
			bool getIsMove();
			void setIsMove(bool isMove);
			Color getColor();
			void setColor(Color color);

		protected:
			TileIndex index;
			Rectangle rec;
			float speed;
			Vector2 destinyPosition;
			bool isMove;
			Color color;
		};
	}
}

#endif // !GAME_OBJECT_H