#include "game_object.h"
#include "scenes/gameplay/gameplay.h"

namespace sokoban
{
	namespace game_object
	{
		GameObject::GameObject()
		{
			this->index = TileIndex();
			this->rec = Rectangle();
			this->speed = 0.0f;
			this->destinyPosition = Vector2();
			this->isMove = false;
			this->color = Color();
		}

		GameObject::GameObject(Rectangle rec, float speed)
		{
			this->index = { 0, 0 };
			this->rec = rec;
			this->speed = speed;
			this->destinyPosition = getPosition();
			this->isMove = false;
			this->color = WHITE;
		}

		GameObject::~GameObject()
		{
		}

		bool GameObject::checkMove(TileIndex auxIndex)
		{
			if (auxIndex.i >= 0 && auxIndex.i < grid::gridRow && auxIndex.j >= 0 && auxIndex.j < grid::gridCol)
			{
				if (grid::tiles[auxIndex.i][auxIndex.j]->getType() == tile::TILE_TYPE::FLOOR
					|| grid::tiles[auxIndex.i][auxIndex.j]->getType() == tile::TILE_TYPE::DEST_BLOCK)
				{
					return true;
				}
			}

			return false;
		}

		void GameObject::changeIndexPosition(TileIndex auxIndex)
		{
			index = auxIndex;

			Vector2 auxPos;
			auxPos.x = (grid::tiles[auxIndex.i][auxIndex.j]->getRec().x + (grid::tiles[auxIndex.i][auxIndex.j]->getRec().width / 2)) - (rec.width / 2);
			auxPos.y = (grid::tiles[auxIndex.i][auxIndex.j]->getRec().y + (grid::tiles[auxIndex.i][auxIndex.j]->getRec().height / 2)) - (rec.height / 2);
			setDestinyPosition(auxPos);

			isMove = true;
		}

		void GameObject::move()
		{
			if (isMove)
			{
				if (rec.y > destinyPosition.y)
				{
					rec.y -= speed * gameplay::frameTime;

					if (rec.y <= destinyPosition.y)
					{
						rec.y = destinyPosition.y;
						isMove = false;
					}

					return;
				}
				if (rec.x > destinyPosition.x)
				{
					rec.x -= speed * gameplay::frameTime;

					if (rec.x <= destinyPosition.x)
					{
						rec.x = destinyPosition.x;
						isMove = false;
					}

					return;
				}
				if (rec.y < destinyPosition.y)
				{
					rec.y += speed * gameplay::frameTime;

					if (rec.y >= destinyPosition.y)
					{
						rec.y = destinyPosition.y;
						isMove = false;
					}

					return;
				}
				if (rec.x < destinyPosition.x)
				{
					rec.x += speed * gameplay::frameTime;

					if (rec.x >= destinyPosition.x)
					{
						rec.x = destinyPosition.x;
						isMove = false;
					}

					return;
				}
			}
		}

		void GameObject::show()
		{
#if DEBUG
			DrawRectangleLinesEx(rec, 1, GREEN);
#endif // DEBUG
		}

		TileIndex GameObject::getIndex()
		{
			return this->index;
		}

		void GameObject::setIndex(TileIndex index)
		{
			this->index = index;
		}

		Rectangle GameObject::getRec()
		{
			return this->rec;
		}

		void GameObject::setRec(Rectangle rec)
		{
			this->rec = rec;
		}

		Vector2 GameObject::getPosition()
		{
			return { this->rec.x, this->rec.y };
		}

		void GameObject::setPosition(Vector2 position)
		{
			this->rec.x = position.x;
			this->rec.y = position.y;
		}

		float GameObject::getSpeed()
		{
			return this->speed;
		}

		void GameObject::setSpeed(float speed)
		{
			this->speed = speed;
		}

		Vector2 GameObject::getDestinyPosition()
		{
			return this->destinyPosition;
		}

		void GameObject::setDestinyPosition(Vector2 destinyPosition)
		{
			this->destinyPosition = destinyPosition;
		}
		bool GameObject::getIsMove()
		{
			return this->isMove;
		}

		void GameObject::setIsMove(bool isMove)
		{
			this->isMove = isMove;
		}

		Color GameObject::getColor()
		{
			return this->color;
		}

		void GameObject::setColor(Color color)
		{
			this->color = color;
		}
	}
}