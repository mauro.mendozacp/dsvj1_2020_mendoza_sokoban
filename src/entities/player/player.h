#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"
#include "entities/animation/animation.h"
#include "entities/game_object/game_object.h"

namespace sokoban
{
	namespace player
	{
		using namespace game_object;

		extern float playerWidth;
		extern float playerHeight;

		void init();
		void start(int i, int j);
		void update();
		void draw();
		void deInit();

		enum class DIRECTION
		{
			UP = 1,
			LEFT,
			DOWN,
			RIGHT
		};

		class Player : public GameObject
		{
		public:
			Player();
			Player(Rectangle rec, float speed);
			~Player();

			void input();
			void start();
			void show();
			void move();
			void changeAnimation();
			void updateAnimation();

			animation::Animation getAnimation();
			DIRECTION getDirection();
			void setDirection(DIRECTION direction);
			int getMoves();
			void setMoves(int moves);
			bool getIsFinish();
			void setIsFinish(bool isFinish);

		private:
			animation::Animation animation;
			DIRECTION direction;
			int moves;
			bool isFinish;
		};
	}
}

#endif // !PLAYER_H