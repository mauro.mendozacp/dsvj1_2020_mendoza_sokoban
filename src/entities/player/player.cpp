#include "player.h"
#include "audio/audio.h"
#include "input/input.h"
#include "texture/texture.h"
#include "scenes/gameplay/gameplay.h"

namespace sokoban
{
	namespace player
	{
		float playerWidth;
		float playerHeight;

		void init()
		{
			playerWidth = tile::tileWidth * 7 / 8;
			playerHeight = tile::tileHeight;

			Rectangle auxRec = Rectangle();
			auxRec.width = playerWidth;
			auxRec.height = playerHeight;
			auxRec.x = 0.0f;
			auxRec.y = 0.0f;

			grid::pj = new player::Player(auxRec, speedObject);
		}

		void start(int i, int j)
		{
			Vector2 auxPos;
			auxPos.x = (grid::tiles[i][j]->getRec().x + (grid::tiles[i][j]->getRec().width / 2)) - (playerWidth / 2);
			auxPos.y = (grid::tiles[i][j]->getRec().y + (grid::tiles[i][j]->getRec().height / 2)) - (playerHeight / 2);

			grid::pj->setPosition(auxPos);
			grid::pj->setIndex({ i, j });
			grid::pj->start();
		}

		void update()
		{
			grid::pj->input();
			grid::pj->move();
			grid::pj->updateAnimation();
		}

		void draw()
		{
			grid::pj->show();
		}

		void deInit()
		{
			if (grid::pj != NULL)
			{
				delete grid::pj;
				grid::pj = NULL;
			}
		}

		Player::Player() : GameObject()
		{
			this->animation = animation::Animation();
			this->direction = DIRECTION();
			this->moves = 0;
			this->isFinish = false;
		}

		Player::Player(Rectangle rec, float speed)
			: GameObject(rec, speed)
		{
			this->animation = animation::Animation();
			this->direction = DIRECTION::DOWN;
			this->moves = 0;
			this->isFinish = false;
		}

		Player::~Player()
		{
		}

		void Player::input()
		{
			if (!isMove && !isFinish)
			{
				if (input::checkPlayerMove())
				{
					TileIndex auxIndex = index;
					TileIndex auxIndexNext = index;

					if (input::checkPlayerMoveUp())
					{
						auxIndex.i--;
						auxIndexNext.i = auxIndex.i - 1;

						setDirection(DIRECTION::UP);
					}
					else if (input::checkPlayerMoveLeft())
					{
						auxIndex.j--;
						auxIndexNext.j = auxIndex.j - 1;

						setDirection(DIRECTION::LEFT);
					}
					else if (input::checkPlayerMoveDown())
					{
						auxIndex.i++;
						auxIndexNext.i = auxIndex.i + 1;

						setDirection(DIRECTION::DOWN);
					}
					else if (input::checkPlayerMoveRight())
					{
						auxIndex.j++;
						auxIndexNext.j = auxIndex.j + 1;

						setDirection(DIRECTION::RIGHT);
					}

					if (checkMove(auxIndex))
					{
						if (block::checkBlock(auxIndex))
						{
							for (block::Block* b : grid::blocks)
							{
								if (b != NULL)
								{
									if (b->getIndex().i == auxIndex.i && b->getIndex().j == auxIndex.j)
									{
										if (b->checkMove(auxIndexNext))
										{
											if (!block::checkBlock(auxIndexNext))
											{
												moves++;
												changeIndexPosition(auxIndex);
												b->changeIndexPosition(auxIndexNext);
											}
										}

										break;
									}
								}
							}
						}
						else
						{
							moves++;
							changeIndexPosition(auxIndex);
						}
					}

					changeAnimation();
				}
			}
		}

		void Player::start()
		{
			moves = 0;
			isMove = false;
			isFinish = false;
			direction = DIRECTION::DOWN;
			changeAnimation();
		}

		void Player::show()
		{
			animation.draw(getPosition(), color);

			GameObject::show();
		}

		void Player::move()
		{
			if (isMove)
			{
				if (!IsSoundPlaying(audio::stepsSound))
				{
					PlaySound(audio::stepsSound);
				}

				if (rec.y > destinyPosition.y)
				{
					rec.y -= speed * gameplay::frameTime;

					if (rec.y <= destinyPosition.y)
					{
						rec.y = destinyPosition.y;
						isMove = false;
						changeAnimation();
					}

					return;
				}
				if (rec.x > destinyPosition.x)
				{
					rec.x -= speed * gameplay::frameTime;

					if (rec.x <= destinyPosition.x)
					{
						rec.x = destinyPosition.x;
						isMove = false;
						changeAnimation();
					}

					return;
				}
				if (rec.y < destinyPosition.y)
				{
					rec.y += speed * gameplay::frameTime;

					if (rec.y >= destinyPosition.y)
					{
						rec.y = destinyPosition.y;
						isMove = false;
						changeAnimation();
					}

					return;
				}
				if (rec.x < destinyPosition.x)
				{
					rec.x += speed * gameplay::frameTime;

					if (rec.x >= destinyPosition.x)
					{
						rec.x = destinyPosition.x;
						isMove = false;
						changeAnimation();
					}

					return;
				}
			}
			else
			{
				if (IsSoundPlaying(audio::stepsSound))
				{
					StopSound(audio::stepsSound);
				}
			}
		}

		void Player::changeAnimation()
		{
			Texture2D auxTexture = Texture2D();
			Rectangle auxPlayerRec = { 0.0f, 0.0f, this->rec.width, this->rec.height };
			int auxFramesX = 0;
			int auxFramesY = 0;
			float auxSpeed = 0.0f;
			int auxResets = 0;
			bool auxIsLoop = false;

			if (!isFinish)
			{
				if (isMove)
				{
					auxFramesX = 6;
					auxFramesY = 2;

					auxSpeed = 600.0f;
					auxIsLoop = true;

					switch (direction)
					{
					case DIRECTION::UP:
						auxTexture = texture::playerMoveBack;
						break;
					case DIRECTION::LEFT:
						auxTexture = texture::playerMoveLeft;
						break;
					case DIRECTION::DOWN:
						auxTexture = texture::playerMoveFront;
						break;
					case DIRECTION::RIGHT:
						auxTexture = texture::playerMoveRight;
						break;
					default:
						break;
					}
				}
				else
				{
					auxFramesX = 6;
					auxFramesY = 3;

					auxSpeed = 600.0f;
					auxIsLoop = true;

					switch (direction)
					{
					case DIRECTION::UP:
						auxTexture = texture::playerIdleBack;
						break;
					case DIRECTION::LEFT:
						auxTexture = texture::playerIdleLeft;
						break;
					case DIRECTION::DOWN:
						auxTexture = texture::playerIdleFront;
						break;
					case DIRECTION::RIGHT:
						auxTexture = texture::playerIdleRight;
						break;
					default:
						break;
					}
				}
			}
			else
			{
				auxFramesX = 6;
				auxFramesY = 2;

				auxSpeed = 1200.0f;
				auxIsLoop = false;

				auxResets = 2;

				auxTexture = texture::playerVictory;
			}

			auxTexture.width = static_cast<int>(auxPlayerRec.width * auxFramesX);
			auxTexture.height = static_cast<int>(auxPlayerRec.height * auxFramesY);

			this->animation = animation::Animation(auxTexture, auxPlayerRec, (auxFramesX * auxFramesY), auxSpeed, auxResets, auxIsLoop);
		}

		void Player::updateAnimation()
		{
			animation.update();
		}

		animation::Animation Player::getAnimation()
		{
			return this->animation;
		}

		DIRECTION Player::getDirection()
		{
			return this->direction;
		}

		void Player::setDirection(DIRECTION direction)
		{
			this->direction = direction;
		}

		int Player::getMoves()
		{
			return this->moves;
		}

		void Player::setMoves(int moves)
		{
			this->moves = moves;
		}

		bool Player::getIsFinish()
		{
			return this->isFinish;
		}

		void Player::setIsFinish(bool isFinish)
		{
			this->isFinish = isFinish;
		}
	}
}