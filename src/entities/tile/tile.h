#ifndef TILE_H
#define TILE_H

#include "raylib.h"

namespace sokoban
{
	namespace tile
	{
		extern float tileWidth;
		extern float tileHeight;

		void init();
		void add(int i, int j);
		void draw();
		void deInit();

		enum class TILE_TYPE
		{
			EMPTY = 1,
			WALL,
			FLOOR,
			DEST_BLOCK
		};

		class Tile
		{
		public:
			Tile();
			Tile(Rectangle rec, TILE_TYPE type);
			~Tile();

			void show();
			void setTextureForType();

			Rectangle getRec();
			void setRec(Rectangle rec);
			Vector2 getPos();
			void setPos(Vector2 pos);
			Texture2D getTexture();
			void setTexture(Texture2D texture);
			Color getColor();
			void setColor(Color color);
			TILE_TYPE getType();
			void setType(TILE_TYPE type);

		private:
			Rectangle rec;
			Texture2D texture;
			Color color;
			TILE_TYPE type;
		};
	}
}

#endif // !TILE_H