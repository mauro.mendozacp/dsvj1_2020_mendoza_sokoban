#include "tile.h"
#include "texture/texture.h"
#include "scenes/gameplay/gameplay.h"

namespace sokoban
{
	namespace tile
	{
		float tileWidth;
		float tileHeight;

		void init()
		{
			tileWidth = grid::gridRec.width / grid::gridCol;
			tileHeight = grid::gridRec.height / grid::gridRow;

			for (int i = 0; i < grid::gridRow; i++)
			{
				for (int j = 0; j < grid::gridCol; j++)
				{
					add(i, j);
				}
			}
		}

		void add(int i, int j)
		{
			Rectangle auxRec = Rectangle();
			auxRec.width = tileWidth;
			auxRec.height = tileHeight;
			auxRec.x = grid::gridRec.x + (j * tileWidth);
			auxRec.y = grid::gridRec.y + (i * tileHeight);

			grid::tiles[i][j] = new Tile(auxRec, TILE_TYPE::EMPTY);
		}

		void draw()
		{
			for (int i = 0; i < grid::gridRow; i++)
			{
				for (int j = 0; j < grid::gridCol; j++)
				{
					grid::tiles[i][j]->show();
				}
			}
		}

		void deInit()
		{
			for (int i = 0; i < grid::gridRow; i++)
			{
				for (int j = 0; j < grid::gridCol; j++)
				{
					if (grid::tiles[i][j] != NULL)
					{
						delete grid::tiles[i][j];
						grid::tiles[i][j] = NULL;
					}
				}
			}
		}

		Tile::Tile()
		{
			this->rec = Rectangle();
			this->texture = Texture2D();
			this->color = Color();
			this->type = TILE_TYPE();
		}

		Tile::Tile(Rectangle rec, TILE_TYPE type)
		{
			this->rec = rec;
			this->texture = Texture2D();
			this->color = WHITE;
			this->type = type;
		}

		Tile::~Tile()
		{
		}

		void Tile::show()
		{
			if (type != TILE_TYPE::WALL)
			{
				DrawTextureEx(texture, getPos(), 0.0f, 1.0f, color);
			}
			else
			{
				Vector2 pos = { getPos().x - rec.width * 5 / 100 , getPos().y - rec.width * 5 / 100 };
				DrawTextureEx(texture, pos, 0.0f, 1.0f, color);
			}

#if DEBUG
			DrawRectangleLinesEx(rec, 1, GREEN);
#endif // DEBUG
		}

		void Tile::setTextureForType()
		{
			int width = static_cast<int>(rec.width + 1);
			int height = static_cast<int>(rec.height + 1);

			switch (type)
			{
			case TILE_TYPE::WALL:
				if (gameplay::level < 6)
				{
					texture = texture::wallLightTile;
				}
				else
				{
					texture = texture::wallDarkTile;
				}
				width += static_cast<int>(rec.width * 10 / 100);
				height += static_cast<int>(rec.height * 10 / 100);
				break;
			case TILE_TYPE::FLOOR:
				if (gameplay::level < 6)
				{
					texture = texture::floorLightTile;
				}
				else
				{
					texture = texture::floorDarkTile;
				}
				break;
			case TILE_TYPE::DEST_BLOCK:
				if (gameplay::level < 6)
				{
					texture = texture::destBlockLightTile;
				}
				else
				{
					texture = texture::destBlockDarkTile;
				}
				break;
			default:
				texture = Texture2D();
				break;
			}

			texture.width = width;
			texture.height = height;
		}

		Rectangle Tile::getRec()
		{
			return this->rec;
		}

		void Tile::setRec(Rectangle rec)
		{
			this->rec = rec;
		}

		Vector2 Tile::getPos()
		{
			return { rec.x, rec.y };
		}

		void Tile::setPos(Vector2 pos)
		{
			rec.x = pos.x;
			rec.y = pos.y;
		}

		Texture2D Tile::getTexture()
		{
			return this->texture;
		}

		void Tile::setTexture(Texture2D texture)
		{
			this->texture = texture;
		}

		Color Tile::getColor()
		{
			return this->color;
		}

		void Tile::setColor(Color color)
		{
			this->color = color;
		}

		TILE_TYPE Tile::getType()
		{
			return this->type;
		}

		void Tile::setType(TILE_TYPE type)
		{
			this->type = type;

			setTextureForType();
		}
	}
}