#ifndef TILE_INDEX_H
#define TILE_INDEX_H

namespace sokoban
{
	struct TileIndex
	{
		int i;
		int j;
	};
}

#endif // !TILE_INDEX_H