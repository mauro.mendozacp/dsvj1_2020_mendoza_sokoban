#ifndef LEVELS_H
#define LEVELS_H

#include "raylib.h"

namespace sokoban
{
	namespace levels
	{
		const int levelsLength = 10;
		extern int levels[levelsLength];
		const int movesLevel[levelsLength] = { 10, 23, 35, 50, 55, 84, 111, 129, 166, 214 };

		void setlevelsDefault();
		void saveLevel(int index, int state);
		void loadLevels();
	}
}

#endif // !LEVELS_H