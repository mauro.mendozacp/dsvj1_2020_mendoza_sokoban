#include "levels.h"
#include <iostream>

namespace sokoban
{
	namespace levels
	{
		int levels[levelsLength] = { 0 };

		void saveLevel(int index, int state)
		{
			if (levels[index] < state)
			{
				SaveStorageValue(index, state);
			}
		}

		void loadLevels()
		{
			if (LoadStorageValue(0) == 0)
			{
				SaveStorageValue(0, 1);
				for (int i = 1; i < levelsLength; i++)
				{
					SaveStorageValue(i, 0);
				}

				levels[0] = LoadStorageValue(0);
			}
			else
			{
				for (int i = 0; i < levelsLength; i++)
				{
					levels[i] = LoadStorageValue(i);
				}
			}
		}

		void setlevelsDefault()
		{
			SaveStorageValue(0, 1);
			for (int i = 1; i < levelsLength; i++)
			{
				SaveStorageValue(i, 0);
			}
		}
	}
}