#include "font.h"

namespace sokoban
{
	namespace font
	{
		Font font;
		float size = 25.0f;
		float spacing = 5.0f;
		Color color = WHITE;

		void init()
		{
			font = LoadFont("res/assets/fonts/general.ttf");
		}

		void deInit()
		{
			UnloadFont(font);
		}

		void drawCenter(Vector2 position, std::string text)
		{
			Vector2 pos;
			pos.x = ((position.x) - (MeasureTextEx(font, &text[0], size, spacing).x / 2));
			pos.y = ((position.y) - (MeasureTextEx(font, &text[0], size, spacing).y / 2) - 3);

			DrawTextEx(font, &text[0], pos, size, spacing, color);
		}
	}
}