#ifndef FONT_H
#define FONT_H

#include "raylib.h"

#include <iostream>

namespace sokoban
{
	namespace font
	{
		extern Font font;
		extern float size;
		extern float spacing;
		extern Color color;

		void init();
		void deInit();
		void drawCenter(Vector2 position, std::string text);
	}
}

#endif // !FONT_H