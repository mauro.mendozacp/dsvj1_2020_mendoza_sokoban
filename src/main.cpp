#include "raylib.h"
#include "game/game.h"

using namespace sokoban;

int main(void)
{
    game::runGame();

    return 0;
}