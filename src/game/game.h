#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <string>

#include "raylib.h"

#include "input/input.h"
#include "font/font.h"
#include "audio/audio.h"
#include "texture/texture.h"

#include "scenes/main_menu/main_menu.h"
#include "scenes/credits/credits.h"
#include "scenes/settings/settings.h"
#include "scenes/level_select/level_select.h"
#include "scenes/gameplay/gameplay.h"
#include "scenes/pause/pause.h"
#include "scenes/level_complete/level_complete.h"

namespace sokoban
{
	namespace game
	{
		enum class GAME_STATUS
		{
			GAMEPLAY = 1,
			MAIN_MENU,
			LEVEL_SELECT,
			PAUSE,
			LEVEL_COMPLETE,
			SETTINGS,
			CREDITS,
			EXIT
		};

		extern GAME_STATUS gameStatus;
		extern std::string titleGame;

		extern int screenWidth;
		extern int screenHeight;
		const int frames = 60;

		void runGame();
		void changeStatus(GAME_STATUS newGameStatus);
	}
}

#endif // !GAME_H