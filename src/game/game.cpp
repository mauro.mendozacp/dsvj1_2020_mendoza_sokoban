#include "game.h"
#include "entities/levels/levels.h"

namespace sokoban
{
	namespace game
	{
		GAME_STATUS gameStatus;
		std::string titleGame = "SOKOBAN";

		int screenWidth = 840;
		int screenHeight = 650;

		static void init()
		{
			InitWindow(screenWidth, screenHeight, &titleGame[0]);
			SetTargetFPS(frames);

			audio::init();
			font::init();
			texture::init();

#if DEBUG
			audio::setMusicVolume(0.0f);
#endif // DEBUG

			gameStatus = GAME_STATUS::MAIN_MENU;
			main_menu::init();
		}

		static void updateAudio()
		{
			switch (gameStatus)
			{
			case GAME_STATUS::MAIN_MENU:
				UpdateMusicStream(audio::menuMusic);
				break;
			case GAME_STATUS::GAMEPLAY:
				UpdateMusicStream(audio::gameplayMusic);
				break;
			case GAME_STATUS::LEVEL_SELECT:
				UpdateMusicStream(audio::menuMusic);
				break;
			case GAME_STATUS::SETTINGS:
				UpdateMusicStream(audio::menuMusic);
				break;
			case GAME_STATUS::CREDITS:
				UpdateMusicStream(audio::menuMusic);
				break;
			default:
				break;
			}
		}

		static void update()
		{
			updateAudio();

			switch (gameStatus)
			{
			case GAME_STATUS::MAIN_MENU:
				main_menu::update();
				break;
			case GAME_STATUS::LEVEL_SELECT:
				level_select::update();
				break;
			case GAME_STATUS::GAMEPLAY:
				gameplay::update();
				break;
			case GAME_STATUS::PAUSE:
				pause::update();
				break;
			case GAME_STATUS::LEVEL_COMPLETE:
				level_complete::update();
				break;
			case GAME_STATUS::SETTINGS:
				settings::update();
				break;
			case GAME_STATUS::CREDITS:
				credits::update();
				break;
			default:
				break;
			}
		}

		static void drawBackground()
		{
			switch (gameStatus)
			{
			case GAME_STATUS::MAIN_MENU:
			case GAME_STATUS::SETTINGS:
			case GAME_STATUS::CREDITS:
			case GAME_STATUS::LEVEL_SELECT:
			case GAME_STATUS::LEVEL_COMPLETE:
				DrawTexture(texture::menuBackground, 0, 0, WHITE);
				break;
			case GAME_STATUS::GAMEPLAY:
			case GAME_STATUS::PAUSE:
				DrawTexture(texture::gameplayBackground, 0, 0, WHITE);
			default:
				break;
			}
		}

		static void draw()
		{
			BeginDrawing();

			ClearBackground(BLACK);

			drawBackground();

			switch (gameStatus)
			{
			case GAME_STATUS::MAIN_MENU:
				main_menu::draw();
				break;
			case GAME_STATUS::LEVEL_SELECT:
				level_select::draw();
				break;
			case GAME_STATUS::GAMEPLAY:
				gameplay::draw();
				break;
			case GAME_STATUS::PAUSE:
				pause::draw();
				break;
			case GAME_STATUS::LEVEL_COMPLETE:
				level_complete::draw();
				break;
			case GAME_STATUS::SETTINGS:
				settings::draw();
				break;
			case GAME_STATUS::CREDITS:
				credits::draw();
				break;
			default:
				break;
			}

			EndDrawing();
		}

		static void deInit()
		{
			CloseWindow();
			audio::deInit();
			font::deInit();
			texture::deInit();
			changeStatus(GAME_STATUS::EXIT);
		}

		void runGame()
		{
			init();
			while (!WindowShouldClose() && gameStatus != GAME_STATUS::EXIT)
			{
				update();
				draw();
			}
			deInit();
		}

		void changeStatus(GAME_STATUS newGameStatus)
		{
			switch (gameStatus)
			{
			case GAME_STATUS::MAIN_MENU:
				main_menu::deInit();
				break;
			case GAME_STATUS::LEVEL_SELECT:
				level_select::deInit();
				break;
			case GAME_STATUS::GAMEPLAY:
				gameplay::deInit();
				break;
			case GAME_STATUS::PAUSE:
				pause::deInit();
				break;
			case GAME_STATUS::LEVEL_COMPLETE:
				level_complete::deInit();
				break;
			case GAME_STATUS::SETTINGS:
				settings::deInit();
				break;
			case GAME_STATUS::CREDITS:
				credits::deInit();
				break;
			default:
				break;
			}

			switch (newGameStatus)
			{
			case GAME_STATUS::MAIN_MENU:
				main_menu::init();
				break;
			case GAME_STATUS::LEVEL_SELECT:
				level_select::init();
				break;
			case GAME_STATUS::GAMEPLAY:
				gameplay::init();
				break;
			case GAME_STATUS::PAUSE:
				pause::init();
				break;
			case GAME_STATUS::LEVEL_COMPLETE:
				level_complete::init();
				break;
			case GAME_STATUS::SETTINGS:
				settings::init();
				break;
			case GAME_STATUS::CREDITS:
				credits::init();
				break;
			default:
				break;
			}

			gameStatus = newGameStatus;
		}
	}
}