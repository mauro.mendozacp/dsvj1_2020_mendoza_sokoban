#ifndef INPUT_H
#define INPUT_H

#include "raylib.h"

namespace sokoban
{
	namespace input
	{
		bool checkClickOnRec(Rectangle rec);

		bool checkPlayerMoveUp();
		bool checkPlayerMoveLeft();
		bool checkPlayerMoveDown();
		bool checkPlayerMoveRight();
		bool checkPlayerMove();
	}
}

#endif // !INPUT_H