#include "input.h"
#include "audio/audio.h"

namespace sokoban
{
	namespace input
	{
		int click = MOUSE_LEFT_BUTTON;

		static bool checkClick()
		{
			if (IsMouseButtonPressed(click))
			{
				return true;
			}

			return false;
		}

		bool checkClickOnRec(Rectangle rec)
		{
			if (CheckCollisionPointRec(GetMousePosition(), rec))
			{
				if (checkClick())
				{
					PlaySound(audio::selectSound);
					return true;
				}
			}

			return false;
		}

		bool checkPlayerMoveUp()
		{
			if (IsKeyPressed(KEY_W) || IsKeyPressed(KEY_UP))
			{
				return true;
			}

			return false;
		}

		bool checkPlayerMoveLeft()
		{
			if (IsKeyPressed(KEY_A) || IsKeyPressed(KEY_LEFT))
			{
				return true;
			}

			return false;
		}

		bool checkPlayerMoveDown()
		{
			if (IsKeyPressed(KEY_S) || IsKeyPressed(KEY_DOWN))
			{
				return true;
			}

			return false;
		}

		bool checkPlayerMoveRight()
		{
			if (IsKeyPressed(KEY_D) || IsKeyPressed(KEY_RIGHT))
			{
				return true;
			}

			return false;
		}

		bool checkPlayerMove()
		{
			if (checkPlayerMoveUp() || checkPlayerMoveLeft() || checkPlayerMoveDown() || checkPlayerMoveRight())
			{
				return true;
			}

			return false;
		}
	}
}