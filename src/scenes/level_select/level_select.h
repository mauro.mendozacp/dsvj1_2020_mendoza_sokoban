#ifndef LEVEL_SELECT_H
#define LEVEL_SELECT_H

#include "raylib.h"

namespace sokoban
{
	namespace level_select
	{
		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !LEVEL_SELECT_H