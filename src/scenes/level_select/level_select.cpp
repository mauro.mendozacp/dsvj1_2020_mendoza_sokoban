#include "level_select.h"
#include <string>
#include "game/game.h"
#include "entities/levels/levels.h"
#include "entities/banner/banner.h"

namespace sokoban
{
	namespace level_select
	{
		banner::Banner* menu;
		std::string title = "LEVEL SELECT";

		enum class LEVEL_SELECT_MENU
		{
			LEVEL_1 = 1,
			LEVEL_2,
			LEVEL_3,
			LEVEL_4,
			LEVEL_5,
			LEVEL_6,
			LEVEL_7,
			LEVEL_8,
			LEVEL_9,
			LEVEL_10,
			RESET,
			BACK
		};
		int const optionLenght = 12;
		banner::Banner* options[optionLenght];

		static void acceptOption(LEVEL_SELECT_MENU option)
		{
			switch (option)
			{
			case LEVEL_SELECT_MENU::LEVEL_1:
				gameplay::level = 1;
				break;
			case LEVEL_SELECT_MENU::LEVEL_2:
				gameplay::level = 2;
				break;
			case LEVEL_SELECT_MENU::LEVEL_3:
				gameplay::level = 3;
				break;
			case LEVEL_SELECT_MENU::LEVEL_4:
				gameplay::level = 4;
				break;
			case LEVEL_SELECT_MENU::LEVEL_5:
				gameplay::level = 5;
				break;
			case LEVEL_SELECT_MENU::LEVEL_6:
				gameplay::level = 6;
				break;
			case LEVEL_SELECT_MENU::LEVEL_7:
				gameplay::level = 7;
				break;
			case LEVEL_SELECT_MENU::LEVEL_8:
				gameplay::level = 8;
				break;
			case LEVEL_SELECT_MENU::LEVEL_9:
				gameplay::level = 9;
				break;
			case LEVEL_SELECT_MENU::LEVEL_10:
				gameplay::level = 10;
				break;
			case LEVEL_SELECT_MENU::RESET:
				levels::setlevelsDefault();
				game::changeStatus(game::GAME_STATUS::LEVEL_SELECT);
				break;
			case LEVEL_SELECT_MENU::BACK:
				game::changeStatus(game::GAME_STATUS::MAIN_MENU);
				break;
			default:
				break;
			}

			if (static_cast<int>(option) > 0 && static_cast<int>(option) < optionLenght - 1)
			{
				if (levels::levels[static_cast<int>(option) - 1] > 0)
				{
					StopMusicStream(audio::menuMusic);
					game::changeStatus(game::GAME_STATUS::GAMEPLAY);
				}
			}
		}

		void init()
		{
			if (!IsMusicPlaying(audio::menuMusic))
			{
				PlayMusicStream(audio::menuMusic);
			}

			levels::loadLevels();

			//Menu
			banner::initMenu(menu, title);

			Vector2 pos;

			Rectangle rec = { 0, 0, banner::menuRecWidth / 8, banner::menuRecWidth / 8 };

			Texture2D texture = texture::level_button;
			texture.width = static_cast<int>(rec.width * 5);
			texture.height = static_cast<int>(rec.height);

			float spacing = banner::menuRecWidth / 16;
			pos.x = menu->getPosition().x + banner::menuRecWidth / 8 - (rec.width / 2);
			pos.y = menu->getPosition().y + banner::menuRecHeight * 3 / 8 - (rec.height / 2);

			for (int i = 0; i < optionLenght - 2; i++)
			{
				if (i != 0)
				{
					pos.x += spacing + banner::menuRecWidth / 8;
				}

				if (i == 5)
				{
					pos.x = menu->getPosition().x + banner::menuRecWidth / 8 - (rec.width / 2);
					pos.y = menu->getPosition().y + banner::menuRecHeight * 5 / 8 - (rec.height / 2);
				}

				rec.x = levels::levels[i] * rec.width;

				options[i] = new banner::Banner(pos, rec, texture, std::to_string(i + 1));
			}

			//Reset
			pos = { menu->getPosition().x + banner::menuRecWidth / 2 - banner::buttonRecWidth / 2,
				menu->getPosition().y + banner::menuRecHeight / 8 };

			rec = { 0, 0, banner::buttonRecWidth, banner::buttonRecHeight };

			Texture2D auxTexture = texture::resetButton;
			auxTexture.width = static_cast<int>(banner::buttonRecWidth);
			auxTexture.height = static_cast<int>(banner::buttonRecHeight * 2);

			options[optionLenght - 2] = new banner::Banner(pos, rec, auxTexture, "");

			//Back
			pos = { static_cast<float>(game::screenWidth / 2) - (banner::optionRecWidth / 2),
				static_cast<float>(banner::menuRecHeight) };

			rec = { 0, 0, banner::optionRecWidth, banner::optionRecHeight };

			texture = texture::optionBanner;
			texture.width = static_cast<int>(banner::optionRecWidth);
			texture.height = static_cast<int>(banner::optionRecHeight * 2);

			options[optionLenght - 1] = new banner::Banner(pos, rec, texture, "BACK");
		}

		void update()
		{
			options[optionLenght - 2]->changeState();
			options[optionLenght - 1]->changeState();
			acceptOption(static_cast<LEVEL_SELECT_MENU>(banner::getOption(options, optionLenght)));
		}

		void draw()
		{
			menu->showTop();

			for (int i = 0; i < optionLenght - 2; i++)
			{
				options[i]->showMidTop();
			}
			options[optionLenght - 2]->show();
			options[optionLenght - 1]->showMid();
		}

		void deInit()
		{
			banner::deInitBanner(menu);
			banner::deInit(options, optionLenght);
		}
	}
}