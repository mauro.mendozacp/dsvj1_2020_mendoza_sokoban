#include "credits.h"
#include "game/game.h"
#include "entities/banner/banner.h"

namespace sokoban
{
	namespace credits
	{
		banner::Banner* back;
		banner::Banner* menu;
		std::string title = "CREDITS";

		static void acceptOption()
		{
			if (input::checkClickOnRec(back->getRecPosition()))
			{
				game::changeStatus(game::GAME_STATUS::MAIN_MENU);
			}
		}

		void init()
		{
			//Menu
			banner::initMenu(menu, title);

			//Credits
			texture::credits.width = static_cast<int>(banner::menuRecWidth * 3 / 4);
			texture::credits.height = static_cast<int>(banner::menuRecHeight * 11 / 16);

			//Back
			Vector2 pos = { static_cast<float>(game::screenWidth / 2) - (banner::optionRecWidth / 2),
				static_cast<float>(banner::menuRecHeight) };

			Rectangle bgTexture = { 0, 0, banner::optionRecWidth, banner::optionRecHeight };

			Texture2D texture = texture::optionBanner;
			texture.width = static_cast<int>(banner::optionRecWidth);
			texture.height = static_cast<int>(banner::optionRecHeight * 2);

			back = new banner::Banner(pos, bgTexture, texture, "BACK");
		}

		void update()
		{
			back->changeState();
			acceptOption();
		}

		void draw()
		{
			menu->showTop();

			Vector2 pos = { menu->getPosition().x + menu->getRec().width / 2 - texture::credits.width / 2,
				menu->getPosition().y + menu->getRec().height / 8 };
			DrawTextureEx(texture::credits, pos, 0.0f, 1.0f, WHITE);

			back->showMid();
		}

		void deInit()
		{
			if (back != NULL)
			{
				delete back;
				back = NULL;
			}

			banner::deInitBanner(menu);
		}
	}
}