#include "main_menu.h"
#include "game/game.h"
#include "entities/banner/banner.h"

namespace sokoban
{
	namespace main_menu
	{
		float titleRecWidth = 500.0f;
		float titleRecHeight = 75.0f;
		float size = 45.0f;
		float spacing = 8.0f;

		enum class MAIN_MENU
		{
			PLAY = 1,
			SETTINGS,
			CREDITS,
			EXIT
		};

		const int optionLenght = 4;
		banner::Banner* options[optionLenght];
		std::string optionTexts[optionLenght] = { "PLAY", "SETTINGS", "CREDITS", "EXIT" };

		banner::Banner* title;

		static void acceptOption(MAIN_MENU option)
		{
			switch (option)
			{
			case MAIN_MENU::PLAY:
				game::changeStatus(game::GAME_STATUS::LEVEL_SELECT);
				break;
			case MAIN_MENU::SETTINGS:
				game::changeStatus(game::GAME_STATUS::SETTINGS);
				break;
			case MAIN_MENU::CREDITS:
				game::changeStatus(game::GAME_STATUS::CREDITS);
				break;
			case MAIN_MENU::EXIT:
				game::changeStatus(game::GAME_STATUS::EXIT);
				break;
			default:
				break;
			}
		}

		void init()
		{
			Rectangle bgTexture = { 0, 0, static_cast<float>(game::screenWidth / 2), titleRecHeight };

			Vector2 pos = { static_cast<float>(game::screenWidth / 2) - (bgTexture.width / 2),
				static_cast<float>(game::screenHeight / 4) - (bgTexture.height / 2) };

			Texture2D texture = texture::titleBanner;
			texture.width = static_cast<int>(bgTexture.width);
			texture.height = static_cast<int>(bgTexture.height);
			title = new banner::Banner(pos, bgTexture, texture, game::titleGame, size, spacing);

			banner::init(options, optionLenght, optionTexts);
		}

		void update()
		{
			banner::update(options, optionLenght);
			
			acceptOption(static_cast<MAIN_MENU>(banner::getOption(options, optionLenght)));

			if (!IsMusicPlaying(audio::menuMusic))
			{
				PlayMusicStream(audio::menuMusic);
			}
		}

		void draw()
		{
			title->showMid();
			banner::draw(options, optionLenght);
		}

		void deInit()
		{
			if (title != NULL)
			{
				delete title;
				title = NULL;
			}

			banner::deInit(options, optionLenght);
		}
	}
}