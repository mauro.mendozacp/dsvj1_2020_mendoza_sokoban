#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "raylib.h"

namespace sokoban
{
	namespace main_menu
	{
		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !MAIN_MENU_H