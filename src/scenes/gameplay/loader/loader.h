#ifndef LOADER_H
#define LOADER_H

#include "raylib.h"

namespace sokoban
{
	namespace loader
	{
		void loadLevel();
	}
}

#endif // !LOADER_H