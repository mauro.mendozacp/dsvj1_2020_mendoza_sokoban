#include "loader.h"
#include "scenes/gameplay/gameplay.h"
#include <string>

namespace sokoban
{
	namespace loader
	{
		const char* LEVELS_PATH = "res/assets/levels/levels.dat";
		const char LEVEL_START = '-';

		void loadLevel()
		{
			//Load level file
			std::string textFile(LoadFileText(LEVELS_PATH));

			//Go level values
			int startingPos = 0;
			int auxLevel = 0;
			while (auxLevel != gameplay::level)
			{
				if (textFile[startingPos] == LEVEL_START)
				{
					auxLevel++;
				}
				startingPos++;
			}
			startingPos++;

			//Set level
			for (int i = 0; i < grid::gridRow; i++)
			{
				for (int j = 0; j < grid::gridCol; j++)
				{
					switch (textFile[startingPos + j + i * (grid::gridCol + 1)])
					{
					case 'W':
						grid::tiles[i][j]->setType(tile::TILE_TYPE::WALL);
						break;
					case 'P':
						player::start(i, j);
						grid::tiles[i][j]->setType(tile::TILE_TYPE::FLOOR);
						break;
					case 'B':
						block::add(i, j);
						grid::tiles[i][j]->setType(tile::TILE_TYPE::FLOOR);
						break;
					case 'D':
						grid::tiles[i][j]->setType(tile::TILE_TYPE::DEST_BLOCK);
						break;
					case 'F':
						grid::tiles[i][j]->setType(tile::TILE_TYPE::FLOOR);
						break;
					case 'X':
						block::add(i, j);
						grid::tiles[i][j]->setType(tile::TILE_TYPE::DEST_BLOCK);
						break;
					default:
						grid::tiles[i][j]->setType(tile::TILE_TYPE::EMPTY);
						break;
					}
				}
			}
		}
	}
}