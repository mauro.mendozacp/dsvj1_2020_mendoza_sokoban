#include "gameplay.h"
#include "game/game.h"
#include "loader/loader.h"
#include "entities/levels/levels.h"

namespace sokoban
{
	namespace gameplay
	{
		banner::Banner* pause;
		banner::Banner* reset;

		int level = 1;
		int stars = 0;

		float frameTime;
		float timer;

		void startLevel()
		{
			timer = 0.0f;
			block::clear();
			loader::loadLevel();

			PlayMusicStream(audio::gameplayMusic);
		}

		static void setStars()
		{
			if (level > 0 && level <= levels::levelsLength)
			{
				if (grid::pj->getMoves() <= levels::movesLevel[level - 1])
				{
					stars = 3;
				}
				else if (grid::pj->getMoves() <= levels::movesLevel[level - 1] * 125 / 100)
				{
					stars = 2;
				}
				else
				{
					stars = 1;
				}
			}
		}

		static void showHUD()
		{
			const char* text = FormatText("MOVES: %i", grid::pj->getMoves());
			Vector2 pos;
			pos.x = static_cast<float>(game::screenWidth / 4);
			pos.y = banner::buttonRecHeight;

			font::drawCenter(pos, text);

			pos.x = static_cast<float>(game::screenWidth * 3 / 4);

			int min = static_cast<int>(timer) / 60;
			int seg = static_cast<int>(timer) % 60;

			if (min < 10)
			{
				if (seg < 10)
				{
					text = FormatText("TIME: 0%i:0%i", min, seg);
				}
				else
				{
					text = FormatText("TIME: 0%i:%i", min, seg);
				}
			}
			else
			{
				if (seg < 10)
				{
					text = FormatText("TIME: %i:0%i", min, seg);
				}
				else
				{
					text = FormatText("TIME: %i:%i", min, seg);
				}
			}

			font::drawCenter(pos, text);
		}

		static void showInstructions()
		{
			DrawTextureEx(texture::instructions,
				{ static_cast<float>(game::screenWidth / 2 - texture::instructions.width / 2), 
				  static_cast<float>(game::screenHeight - texture::instructions.height * 125 / 100) },
				0.0f, 1.0f, WHITE);
		}

		static void pauseGame()
		{
			pause->changeState();
			if (input::checkClickOnRec(pause->getRecPosition()))
			{
				pause::init();
				game::gameStatus = game::GAME_STATUS::PAUSE;
			}
		}

		static void resetGame()
		{
			reset->changeState();
			if (input::checkClickOnRec(reset->getRecPosition()))
			{
				startLevel();
			}
		}

		static void finish()
		{
			grid::pj->setIsFinish(true);
			grid::pj->changeAnimation();

			for (block::Block* b : grid::blocks)
			{
				if (b != NULL)
				{
					b->changeState(block::BLOCK_STATE::FINISH);
				}
			}

			PlaySound(audio::victorySound);
			StopMusicStream(audio::gameplayMusic);
		}

		static void finishGame()
		{
			if (!grid::pj->getIsFinish())
			{
				if (grid::checkBlocksInDestiny())
				{
					if (!grid::pj->getIsMove())
					{
						finish();
					}
				}

#if DEBUG
				if (IsKeyPressed(KEY_P))
				{
					finish();
				}
#endif // DEBUG

			}
			else
			{
				if (grid::pj->getAnimation().getIsFinish())
				{
					setStars();
					levels::saveLevel(level - 1, stars + 1);

					if (level < levels::levelsLength)
					{
						levels::saveLevel(level, 1);
					}

					level_complete::init();
					game::gameStatus = game::GAME_STATUS::LEVEL_COMPLETE;
				}
			}
		}

		void init()
		{
			texture::initGameplay();
			grid::init();
			banner::gameplayButtonsInit();

			frameTime = 0.0f;
			timer = 0.0f;

			loader::loadLevel();

			if (level == 1)
			{
				texture::instructions.width = game::screenWidth * 3 / 8;
				texture::instructions.height = game::screenWidth / 8;
			}

			PlayMusicStream(audio::gameplayMusic);
		}

		void update()
		{
			frameTime = GetFrameTime();
			
			if (!grid::pj->getIsFinish())
			{
				timer += frameTime;

				pauseGame();
				resetGame();
			}

			grid::update();

			finishGame();
		}

		void draw()
		{
			grid::draw();
			pause->show();
			reset->show();
			showHUD();

			if (level == 1)
			{
				showInstructions();
			}
			else
			{
#if DEBUG
				font::drawCenter({ static_cast<float>(game::screenWidth / 2), static_cast<float>(game::screenHeight * 7 / 8) },
					"MODE DEBUG: P (SKIP LEVEL)");
#endif // DEBUG
			}
		}

		void deInit()
		{
			texture::deInitGameplay();
			grid::deInit();
			banner::deInitBanner(pause);
			banner::deInitBanner(reset);
		}
	}
}