#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"
#include "entities/banner/banner.h"
#include "scenes/gameplay/grid/grid.h"

namespace sokoban
{
	namespace gameplay
	{
		extern banner::Banner* pause;
		extern banner::Banner* reset;

		extern int level;
		extern int stars;

		extern float frameTime;
		extern float timer;

		void startLevel();

		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !GAMEPLAY_H