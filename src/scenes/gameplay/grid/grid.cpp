#include "grid.h"
#include "game/game.h"

namespace sokoban
{
	namespace grid
	{
		Rectangle gridRec;

		tile::Tile* tiles[gridRow][gridCol];

		player::Player* pj;
		std::vector<block::Block*> blocks;

		bool checkBlocksInDestiny()
		{
			for (block::Block* b : grid::blocks)
			{
				if (b != NULL)
				{
					if (b->GetBlockState() != block::BLOCK_STATE::ON_DESTINATION)
					{
						return false;
					}
				}
			}

			return true;
		}

		void init()
		{
			gridRec.width = static_cast<float>(game::screenWidth * 3 / 4);
			gridRec.height = static_cast<float>(game::screenHeight * 3 / 4);
			gridRec.x = static_cast<float>((game::screenWidth - gridRec.width) / 2);
			gridRec.y = static_cast<float>((game::screenHeight - gridRec.height) / 2);

			tile::init();
			player::init();
			block::init();
		}

		void update()
		{
			player::update();
			
			if (!pj->getIsFinish())
			{
				block::update();
			}
		}

		void draw()
		{
			tile::draw();
			player::draw();
			block::draw();
		}

		void deInit()
		{
			tile::deInit();
			player::deInit();
			block::clear();
		}
	}
}