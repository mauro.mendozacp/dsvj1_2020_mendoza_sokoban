#ifndef GRID_H
#define GRID_H

#include <iostream>
#include <vector>

#include "raylib.h"
#include "entities/tile/tile.h"
#include "entities/block/block.h"
#include "entities/player/player.h"

namespace sokoban
{
	namespace grid
	{
		extern Rectangle gridRec;

		const int gridRow = 12;
		const int gridCol = 16;
		extern tile::Tile* tiles[gridRow][gridCol];
		
		extern player::Player* pj;
		extern std::vector<block::Block*> blocks;

		bool checkBlocksInDestiny();

		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !GRID_