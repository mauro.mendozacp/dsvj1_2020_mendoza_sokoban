#ifndef LEVEL_COMPLETE_H
#define LEVEL_COMPLETE_H

#include "raylib.h"

namespace sokoban
{
	namespace level_complete
	{
		void init();
		void update();
		void draw();
		void deInit();
	}
}

#endif // !LEVEL_COMPLETE_H