#include "level_complete.h"
#include "game/game.h"
#include "entities/levels/levels.h"
#include "entities/banner/banner.h"

namespace sokoban
{
	namespace level_complete
	{
		enum class LEVEL_COMPLETE_MENU
		{
			GO_BACK = 1,
			CONTINUE
		};

		banner::Banner* menu;

		banner::Banner* stars;
		banner::Banner* moves;
		banner::Banner* time;

		const int optionLenght = 2;
		banner::Banner* options[optionLenght];

		static void acceptOption(LEVEL_COMPLETE_MENU option)
		{
			switch (option)
			{
			case LEVEL_COMPLETE_MENU::GO_BACK:
				gameplay::deInit();
				game::changeStatus(game::GAME_STATUS::LEVEL_SELECT);
				break;
			case LEVEL_COMPLETE_MENU::CONTINUE:
				
				if (gameplay::level < levels::levelsLength)
				{
					gameplay::level++;
					game::gameStatus = game::GAME_STATUS::GAMEPLAY;
					gameplay::startLevel();
					deInit();
				}
				else
				{
					gameplay::deInit();
					game::changeStatus(game::GAME_STATUS::LEVEL_SELECT);
				}
				break;
			default:
				break;
			}
		}

		void init()
		{
			std::string title = FormatText("LEVEL %i COMPLETE!", gameplay::level);
			banner::initMenu(menu, title);

			//Stars
			Rectangle rec = { 0, 0, banner::menuRecWidth / 2,  banner::menuRecHeight / 4 };

			Vector2 pos;
			pos.x = menu->getPosition().x + banner::menuRecWidth / 2 - (rec.width / 2);
			pos.y = menu->getPosition().x + banner::menuRecHeight / 4 - (rec.height / 2);

			Texture2D texture = texture::stars;
			texture.width = static_cast<int>(rec.width);
			texture.height = static_cast<int>(rec.height * 3);

			rec.y = (gameplay::stars - 1) * rec.height;

			stars = new banner::Banner(pos, rec, texture, "");

			//Moves Banner
			rec = { 0, 0, banner::menuRecWidth * 3 / 4, banner::menuRecHeight / 8 };

			pos.x = menu->getPosition().x + banner::menuRecWidth / 2 - (rec.width / 2);
			pos.y = menu->getPosition().y + banner::menuRecHeight * 5 / 8 - (rec.height * 125 / 100);

			texture = texture::movesBanner;
			texture.width = static_cast<int>(rec.width);
			texture.height = static_cast<int>(rec.height);

			moves = new banner::Banner(pos, rec, texture, std::to_string(grid::pj->getMoves()));

			//Time Banner
			pos.x = menu->getPosition().x + banner::menuRecWidth / 2 - (rec.width / 2);
			pos.y = menu->getPosition().y + banner::menuRecHeight * 5 / 8 + (rec.height * 25 / 100);

			texture = texture::timeBanner;
			texture.width = static_cast<int>(rec.width);
			texture.height = static_cast<int>(rec.height);

			const char* text;

			int min = static_cast<int>(gameplay::timer) / 60;
			int seg = static_cast<int>(gameplay::timer) % 60;

			if (min < 10)
			{
				if (seg < 10)
				{
					text = FormatText("0%i:0%i", min, seg);
				}
				else
				{
					text = FormatText("0%i:%i", min, seg);
				}
			}
			else
			{
				if (seg < 10)
				{
					text = FormatText("%i:0%i", min, seg);
				}
				else
				{
					text = FormatText("%i:%i", min, seg);
				}
			}

			time = new banner::Banner(pos, rec, texture, text);

			//Exit Button
			rec = { 0, 0, banner::buttonRecWidth * 2, banner::buttonRecHeight * 2 };

			pos.x = menu->getPosition().x + banner::menuRecWidth / 2 - (rec.width * 125 / 100);
			pos.y = menu->getPosition().y + banner::menuRecHeight - (rec.height / 2);

			texture = texture::exitButton;
			texture.width = static_cast<int>(rec.width);
			texture.height = static_cast<int>(rec.height * 2);

			options[0] = new banner::Banner(pos, rec, texture, "");

			//Continue Button
			pos.x = menu->getPosition().x + banner::menuRecWidth * 5 / 8 - (rec.width / 2);

			texture = texture::continueButton;
			texture.width = static_cast<int>(rec.width);
			texture.height = static_cast<int>(rec.height * 2);

			options[1] = new banner::Banner(pos, rec, texture, "");	
		}

		void update()
		{
			banner::update(options, optionLenght);
			acceptOption(static_cast<LEVEL_COMPLETE_MENU>(banner::getOption(options, optionLenght)));
		}

		void draw()
		{
			menu->showTop();
			stars->show();
			moves->showMidRight();
			time->showMidRight();
			banner::draw(options, optionLenght);
		}

		void deInit()
		{
			banner::deInitBanner(menu);
			banner::deInit(options, optionLenght);
			banner::deInitBanner(stars);
			banner::deInitBanner(moves);
			banner::deInitBanner(time);

			if (IsSoundPlaying(audio::victorySound))
			{
				StopSound(audio::victorySound);
			}
		}
	}
}