#include "settings.h"
#include "game/game.h"
#include "entities/banner/banner.h"

namespace sokoban
{
	namespace settings
	{
		banner::Banner* menu;
		std::string title = "SETTINGS";

		enum class SETTINGS_MENU
		{
			RESOLUTION = 1,
			MUSIC,
			SFX,
			BACK
		};

		const int optionLenght = 4;
		banner::Banner* options[optionLenght];
		std::string optionTexts[optionLenght] = { "RESOLUTION", "MUSIC", "SFX", "BACK" };
		Vector2 textsPosition[optionLenght - 1];

		const int resolutionLenght = 3;
		Vector2 resolutionValues[resolutionLenght] = { { 600, 400 }, { 840, 650 }, { 1240, 800 } };
		std::string resolutionsText[resolutionLenght] = { "600x400", "840x650", "1240x800" };
		float fontOptions[resolutionLenght] = { 15, 25, 35 };
		Vector2 optionRecSize[resolutionLenght] = { { 200.0f, 30.0f }, { 300.0f, 50.0f }, { 400.0f, 75.0f } };
		Vector2 pauseRecSize[resolutionLenght] = { { 35.0f, 35.0f }, { 50.0f, 50.0f }, { 75.0f, 75.0f } };

		const int volumeLenght = 2;
		float musicValue[volumeLenght] = { 0.0f, audio::musicDefaultVolume };
		float sfxValue[volumeLenght] = { 0.0f, audio::sfxDefaultVolume };

		int valueIndexes[optionLenght - 1] = { 0, 0, 0 };

		static int getNextValue(int index, int lenght)
		{
			index++;

			if (index < lenght)
			{
				return index;
			}

			return 0;
		}

		static void setValues()
		{
			for (int i = 0; i < resolutionLenght; i++)
			{
				if (game::screenWidth == resolutionValues[i].x && game::screenHeight == resolutionValues[i].y)
				{
					valueIndexes[0] = i;
					options[0]->setText(resolutionsText[i]);
					break;
				}
			}

			if (audio::musicVolume == 0.0f)
			{
				valueIndexes[1] = 0;
			}
			else
			{
				valueIndexes[1] = 1;
			}

			if (audio::sfxVolume == 0.0f)
			{
				valueIndexes[2] = 0;
			}
			else
			{
				valueIndexes[2] = 1;
			}

			options[1]->setState(valueIndexes[1]);
			options[2]->setState(valueIndexes[2]);
		}

		static void acceptOption(SETTINGS_MENU option)
		{
			int index = 0;

			switch (option)
			{
			case SETTINGS_MENU::RESOLUTION:
				index = getNextValue(valueIndexes[0], resolutionLenght);
				game::screenWidth = static_cast<int>(resolutionValues[index].x);
				game::screenHeight = static_cast<int>(resolutionValues[index].y);
				font::size = fontOptions[index];
				banner::menuRecWidth = static_cast<float>(game::screenWidth * 3 / 4);
				banner::menuRecHeight = static_cast<float>(game::screenHeight * 3 / 4);
				banner::optionRecWidth = optionRecSize[index].x;
				banner::optionRecHeight = optionRecSize[index].y;
				banner::buttonRecWidth = pauseRecSize[index].x;
				banner::buttonRecHeight = pauseRecSize[index].y;
				texture::setBackgroundSizes();
				game::changeStatus(game::GAME_STATUS::SETTINGS);
				SetWindowSize(game::screenWidth, game::screenHeight);

				break;
			case SETTINGS_MENU::MUSIC:
				index = getNextValue(valueIndexes[1], volumeLenght);
				audio::setMusicVolume(musicValue[index]);
				options[1]->setState(index);

				break;
			case SETTINGS_MENU::SFX:
				index = getNextValue(valueIndexes[2], volumeLenght);
				audio::setSfxVolume(sfxValue[index]);
				options[2]->setState(index);

				break;
			case SETTINGS_MENU::BACK:
				game::changeStatus(game::GAME_STATUS::MAIN_MENU);
				break;
			default:
				break;
			}

			if (static_cast<int>(option) > 0 && static_cast<int>(option) < 4)
			{
				setValues();
			}
		}

		void init()
		{
			//Menu
			banner::initMenu(menu, title);

			//Options
			Vector2 pos;

			Rectangle bgTexture = { 0, 0, banner::optionRecWidth * 3 / 4, banner::optionRecHeight };

			Texture2D texture;

			for (int i = 0; i < optionLenght - 1; i++)
			{
				textsPosition[i].x = banner::menuRecWidth / 2;
				textsPosition[i].y = banner::menuRecHeight / 2 + (i * banner::optionRecHeight * 3 / 2);

				if (i == 0)
				{
					bgTexture = { 0, 0, banner::optionRecWidth * 3 / 4, banner::optionRecHeight };
					texture = texture::optionBanner;
					texture.width = static_cast<int>(bgTexture.width);
					texture.height = static_cast<int>(bgTexture.height * 2);
				}
				else
				{
					bgTexture = { 0, 0, banner::optionRecWidth / 2, banner::optionRecHeight };
					texture = texture::power_on_off;
					texture.width = static_cast<int>(bgTexture.width);
					texture.height = static_cast<int>(bgTexture.height * 2);
				}

				pos = { banner::menuRecWidth * 7 / 8 - (bgTexture.width / 2),
				textsPosition[i].y - (bgTexture.height / 2) };

				options[i] = new banner::Banner(pos, bgTexture, texture, "");
			}

			//Back
			pos = { static_cast<float>(game::screenWidth / 2) - (banner::optionRecWidth / 2),
				static_cast<float>(banner::menuRecHeight) };

			bgTexture = { 0, 0, banner::optionRecWidth, banner::optionRecHeight };

			texture = texture::optionBanner;
			texture.width = static_cast<int>(banner::optionRecWidth);
			texture.height = static_cast<int>(banner::optionRecHeight * 2);

			options[optionLenght - 1] = new banner::Banner(pos, bgTexture, texture, optionTexts[optionLenght - 1]);

			setValues();
		}

		void update()
		{
			options[0]->changeState();
			options[optionLenght - 1]->changeState();
			acceptOption(static_cast<SETTINGS_MENU>(banner::getOption(options, optionLenght)));
		}

		void draw()
		{
			menu->showTop();

			for (int i = 0; i < optionLenght - 1; i++)
			{
				font::drawCenter(textsPosition[i], optionTexts[i]);
			}

			banner::draw(options, optionLenght);
		}

		void deInit()
		{
			banner::deInitBanner(menu);
			banner::deInit(options, optionLenght);
		}
	}
}