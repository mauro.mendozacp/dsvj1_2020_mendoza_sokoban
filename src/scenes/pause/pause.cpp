#include "pause.h"
#include "game/game.h"
#include "entities/banner/banner.h"

namespace sokoban
{
	namespace pause
	{
		enum class PAUSE_MENU
		{
			RESUME = 1,
			GO_BACK
		};

		const int optionLenght = 2;
		banner::Banner* menu;
		banner::Banner* options[optionLenght];

		std::string title = "PAUSE";
		std::string optionTexts[optionLenght] = { "RESUME", "BACK TO MENU" };

		static void acceptOption(PAUSE_MENU option)
		{
			switch (option)
			{
			case PAUSE_MENU::RESUME:
				ResumeMusicStream(audio::gameplayMusic);
				game::gameStatus = game::GAME_STATUS::GAMEPLAY;
				deInit();
				break;
			case PAUSE_MENU::GO_BACK:
				StopMusicStream(audio::gameplayMusic);
				gameplay::deInit();
				game::changeStatus(game::GAME_STATUS::LEVEL_SELECT);
				break;
			default:
				break;
			}
		}

		void init()
		{
			PauseMusicStream(audio::gameplayMusic);
			banner::initMenu(menu, title);
			banner::init(options, optionLenght, optionTexts);
		}

		void update()
		{
			banner::update(options, optionLenght);
			acceptOption(static_cast<PAUSE_MENU>(banner::getOption(options, optionLenght)));
		}

		void draw()
		{
			menu->showTop();
			banner::draw(options, optionLenght);
		}

		void deInit()
		{
			banner::deInitBanner(menu);
			banner::deInit(options, optionLenght);
		}
	}
}