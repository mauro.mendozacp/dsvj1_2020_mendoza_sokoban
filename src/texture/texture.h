#ifndef TEXTURE_H
#define TEXTURE_H

#include "raylib.h"

namespace sokoban
{
	namespace texture
	{
		extern Texture2D menuBackground;
		extern Texture2D menuBanner;
		extern Texture2D optionBanner;
		extern Texture2D titleBanner;
		extern Texture2D credits;
		extern Texture2D power_on_off;
		extern Texture2D level_button;

		extern Texture2D gameplayBackground;
		extern Texture2D pauseButton;
		extern Texture2D resetButton;
		extern Texture2D stars;
		extern Texture2D exitButton;
		extern Texture2D continueButton;
		extern Texture2D movesBanner;
		extern Texture2D timeBanner;
		extern Texture2D instructions;

		extern Texture2D floorLightTile;
		extern Texture2D floorDarkTile;
		extern Texture2D wallLightTile;
		extern Texture2D wallDarkTile;
		extern Texture2D destBlockLightTile;
		extern Texture2D destBlockDarkTile;
		extern Texture2D blockOff;
		extern Texture2D blockOn;
		extern Texture2D blockFinish;

		extern Texture2D playerIdleFront;
		extern Texture2D playerIdleLeft;
		extern Texture2D playerIdleBack;
		extern Texture2D playerIdleRight;

		extern Texture2D playerMoveFront;
		extern Texture2D playerMoveLeft;;
		extern Texture2D playerMoveBack;;
		extern Texture2D playerMoveRight;

		extern Texture2D playerVictory;

		void init();
		void setBackgroundSizes();
		void deInit();

		void initGameplay();
		void deInitGameplay();
	}
}

#endif // !TEXTURE_H