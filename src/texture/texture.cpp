#include "texture.h"
#include "game/game.h"

namespace sokoban
{
	namespace texture
	{
		Texture2D menuBackground;
		Texture2D menuBanner;
		Texture2D optionBanner;
		Texture2D titleBanner;
		Texture2D credits;
		Texture2D power_on_off;
		Texture2D level_button;

		Texture2D gameplayBackground;
		Texture2D pauseButton;
		Texture2D resetButton;
		Texture2D stars;
		Texture2D exitButton;
		Texture2D continueButton;
		Texture2D movesBanner;
		Texture2D timeBanner;
		Texture2D instructions;

		Texture2D floorLightTile;
		Texture2D floorDarkTile;
		Texture2D wallLightTile;
		Texture2D wallDarkTile;
		Texture2D destBlockLightTile;
		Texture2D destBlockDarkTile;
		Texture2D blockOff;
		Texture2D blockOn;
		Texture2D blockFinish;
		
		Texture2D playerIdleFront;
		Texture2D playerIdleLeft;
		Texture2D playerIdleBack;
		Texture2D playerIdleRight;

		Texture2D playerMoveFront;
		Texture2D playerMoveLeft;;
		Texture2D playerMoveBack;;
		Texture2D playerMoveRight;

		Texture2D playerVictory;

		void init()
		{
			menuBackground = LoadTexture("res/assets/textures/menu_background.png");
			setBackgroundSizes();

			menuBanner = LoadTexture("res/assets/textures/menu.png");
			optionBanner = LoadTexture("res/assets/textures/option.png");
			titleBanner = LoadTexture("res/assets/textures/title.png");
			credits = LoadTexture("res/assets/textures/credits.png");
			power_on_off = LoadTexture("res/assets/textures/power_on_off.png");
			level_button = LoadTexture("res/assets/textures/level_button.png");
			resetButton = LoadTexture("res/assets/textures/reset.png");
		}

		void initGameplay()
		{
			gameplayBackground = LoadTexture("res/assets/textures/gameplay_background.png");
			gameplayBackground.width = game::screenWidth;
			gameplayBackground.height = game::screenHeight;

			pauseButton = LoadTexture("res/assets/textures/pause.png");
			stars = LoadTexture("res/assets/textures/stars.png");
			exitButton = LoadTexture("res/assets/textures/exit.png");
			continueButton = LoadTexture("res/assets/textures/continue.png");
			movesBanner = LoadTexture("res/assets/textures/moves_banner.png");
			timeBanner = LoadTexture("res/assets/textures/time_banner.png");
			instructions = LoadTexture("res/assets/textures/instructions.png");

			floorLightTile = LoadTexture("res/assets/textures/floor_light.png");
			floorDarkTile = LoadTexture("res/assets/textures/floor_dark.png");
			wallLightTile = LoadTexture("res/assets/textures/wall_light.png");
			wallDarkTile = LoadTexture("res/assets/textures/wall_dark.png");
			destBlockLightTile = LoadTexture("res/assets/textures/destiny_block_light.png");
			destBlockDarkTile = LoadTexture("res/assets/textures/destiny_block_dark.png");
			blockOff = LoadTexture("res/assets/textures/block_off.png");
			blockOn = LoadTexture("res/assets/textures/block_on.png");
			blockFinish = LoadTexture("res/assets/textures/block_finish.png");

			playerIdleFront = LoadTexture("res/assets/textures/player_idle_front.png");
			playerIdleLeft = LoadTexture("res/assets/textures/player_idle_left.png");
			playerIdleBack = LoadTexture("res/assets/textures/player_idle_back.png");
			playerIdleRight = LoadTexture("res/assets/textures/player_idle_right.png");

			playerMoveFront = LoadTexture("res/assets/textures/player_move_front.png");
			playerMoveLeft = LoadTexture("res/assets/textures/player_move_left.png");
			playerMoveBack = LoadTexture("res/assets/textures/player_move_back.png");
			playerMoveRight = LoadTexture("res/assets/textures/player_move_right.png");

			playerVictory = LoadTexture("res/assets/textures/player_victory.png");
		}

		void setBackgroundSizes()
		{
			menuBackground.width = game::screenWidth;
			menuBackground.height = game::screenHeight;
		}

		void deInit()
		{
			UnloadTexture(menuBackground);
			UnloadTexture(menuBanner);
			UnloadTexture(optionBanner);
			UnloadTexture(titleBanner);
			UnloadTexture(credits);
			UnloadTexture(power_on_off);
			UnloadTexture(level_button);
			UnloadTexture(resetButton);
		}

		void deInitGameplay()
		{
			UnloadTexture(pauseButton);
			UnloadTexture(stars);
			UnloadTexture(exitButton);
			UnloadTexture(continueButton);
			UnloadTexture(movesBanner);
			UnloadTexture(timeBanner);
			UnloadTexture(instructions);

			UnloadTexture(floorLightTile);
			UnloadTexture(floorDarkTile);
			UnloadTexture(wallLightTile);
			UnloadTexture(wallDarkTile);
			UnloadTexture(destBlockLightTile);
			UnloadTexture(destBlockDarkTile);
			UnloadTexture(blockOff);
			UnloadTexture(blockOn);
			UnloadTexture(blockFinish);

			UnloadTexture(playerIdleFront);
			UnloadTexture(playerIdleLeft);
			UnloadTexture(playerIdleBack);
			UnloadTexture(playerIdleRight);

			UnloadTexture(playerMoveFront);
			UnloadTexture(playerMoveLeft);
			UnloadTexture(playerMoveBack);
			UnloadTexture(playerMoveRight);

			UnloadTexture(playerVictory);
		}
	}
}