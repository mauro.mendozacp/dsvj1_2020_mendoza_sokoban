#include "audio.h"

namespace sokoban
{
	namespace audio
	{
		Music gameplayMusic;
		Music menuMusic;

		Sound selectSound;
		Sound stepsSound;
		Sound blockSound;
		Sound openChestSound;
		Sound victorySound;

		const float musicDefaultVolume = 0.6f;
		const float sfxDefaultVolume = 0.8f;

		float musicVolume = musicDefaultVolume;
		float sfxVolume = sfxDefaultVolume;

		void init()
		{
			InitAudioDevice();

			gameplayMusic = LoadMusicStream("res/assets/music/gameplay-soundtrack.mp3");
			menuMusic = LoadMusicStream("res/assets/music/menu-soundtrack.mp3");

			selectSound = LoadSound("res/assets/sounds/option.ogg");
			stepsSound = LoadSound("res/assets/sounds/steps.ogg");
			blockSound = LoadSound("res/assets/sounds/block.ogg");
			openChestSound = LoadSound("res/assets/sounds/open_chest.ogg");
			victorySound = LoadSound("res/assets/sounds/victory.ogg");

			setMusicVolume(musicDefaultVolume);
			setSfxVolume(sfxDefaultVolume);
		}

		void deInit()
		{
			UnloadMusicStream(gameplayMusic);
			UnloadMusicStream(menuMusic);

			UnloadSound(selectSound);
			UnloadSound(stepsSound);
			UnloadSound(blockSound);
			UnloadSound(openChestSound);
			UnloadSound(victorySound);

			CloseAudioDevice();
		}

		void setMusicVolume(float volume)
		{
			musicVolume = volume;
			SetMusicVolume(gameplayMusic, volume);
			SetMusicVolume(menuMusic, volume);
		}

		void setSfxVolume(float volume)
		{
			sfxVolume = volume;
			SetSoundVolume(selectSound, volume);
			SetSoundVolume(stepsSound, volume);
			SetSoundVolume(blockSound, volume);
			SetSoundVolume(openChestSound, volume);
			SetSoundVolume(victorySound, volume);
		}
	}
}