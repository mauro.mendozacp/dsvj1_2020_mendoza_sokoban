#ifndef AUDIO_H
#define AUDIO_H

#include "raylib.h"

namespace sokoban
{
	namespace audio
	{
		extern Music gameplayMusic;
		extern Music menuMusic;

		extern Sound selectSound;
		extern Sound stepsSound;
		extern Sound blockSound;
		extern Sound openChestSound;
		extern Sound victorySound;

		extern const float musicDefaultVolume;
		extern const float sfxDefaultVolume;
		extern float musicVolume;
		extern float sfxVolume;

		void init();
		void deInit();
		void setMusicVolume(float volume);
		void setSfxVolume(float volume);
	}
}

#endif // !AUDIO_H